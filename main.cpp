#include <cstdint>
#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <iterator>
#include <thread>
#include <sys/types.h>
#include <sys/stat.h>
#include "aes.h"
#include "sha1.h"

using buffer = std::vector<unsigned char>;

size_t get_file_length(const string& file) {
    struct stat filestatus;
    stat(file.c_str(), &filestatus);
    return filestatus.st_size;
}

bool stream_good(const std::fstream& is) {
    return is.is_open() && is.good();
}

buffer get_buffer(const string& filename) {
    std::fstream is(filename.c_str(), std::ios::in |
                                      std::ios::binary);
    buffer buf;

    if(stream_good(is)) {
        size_t filesize = get_file_length(filename);
        buf.resize(filesize);
        is.read(reinterpret_cast<char*>(buf.data()), filesize);
        is.close();
    } else {
        cout << "Error opening file or his state is bad\n";
    }

    return buf;
}


void encrypt(const string& filename, unsigned char* key) {
    buffer buf = get_buffer(filename);
    unsigned int len = 0;

    unsigned char* chipher = AES(192).EncryptECB(buf.data(), buf.size(), key, len);

    std::fstream ostream(filename.c_str(), std::ios::out |
                                           std::ios::binary);
    ostream.write((char*)chipher, len);
    cout << "File encrypted\n";
}

void decrypt(const string& filename, unsigned char* key) {
    buffer buf = get_buffer(filename);

    unsigned char* chipher = AES(192).DecryptECB(buf.data(), buf.size(), key);
    std::fstream ostream(filename.c_str(), std::ios::out |
                                           std::ios::binary);
    ostream.write((char*)chipher, buf.size());
    cout << "File decrypted\n";
}

void interactive_mode() {
    string file;
    string password;

    cout << "Enter file: ";
    std::cin >> file;

    char choosen;
    cout << "Encrypt [y/n?] ";
    std::cin >> choosen;

    cout << "Enter password: ";
    std::cin >> password;


    SHA1 hashgen;
    hashgen.update(password);
    string hash = hashgen.final();

    string hash_key = hash.substr(29, 44);
    unsigned char* key = reinterpret_cast<unsigned char*>(hash_key.data());

    if(tolower(choosen) == 'y') {
        encrypt(file, key);
    } else if(tolower(choosen) == 'n') {
        decrypt(file, key);
    } else {
        cout << "Unknown choose\n";
    }

}

int main(int argc, char** argv)
{
    string file;
    string password;
    string mode;

    switch (argc) {
    case 1: {
        cout << "========Interactive mode========\n";
        mode = "interactive";
        break;
    }
    case 2:
        mode = argv[1];

        cout << "Enter file: ";
        std::cin >> file;

        cout << "Enter password: ";
        std::cin >> password;

        break;
    case 4:
        mode = argv[1];
        file = argv[2];
        password = argv[3];
        break;
    }
    SHA1 hashgen;
    hashgen.update(password);
    string hash = hashgen.final();

    string hash_key = hash.substr(29, 44);
    unsigned char* key = reinterpret_cast<unsigned char*>(hash_key.data());

    if(mode == "encrypt")
        encrypt(file, key);
    else if(mode == "decrypt")
        decrypt(file, key);
    else if(mode == "interactive")
        interactive_mode();
    else
        std::cout << "Undefined mode\n";

    return EXIT_SUCCESS;

}
